package com.caducidadarchivos.base;

import java.io.Serializable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class EntidadJson implements Serializable {

	private static final long serialVersionUID = 5351139665671968284L;

	public JsonObject toJsonObject() {
		return (JsonObject) new Gson().toJsonTree(this);
	}

	public String toJsonIndentado() {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(this);
	}

}
