package com.caducidadarchivos.entidad;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Set;

import com.caducidadarchivos.base.EntidadJson;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Datos extends EntidadJson {
	private static final long serialVersionUID = -1778836445305738556L;

	private String nombre;
	
	private LocalDate fecha;
	
	private Set<Directorio> directorios;
	
	private boolean finTarea;

	public String getFechaParseada() {
		return fecha.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
	}

	@Override
	public String toString() {
		return "Datos [nombre=" + nombre + ", fecha=" + fecha + ", directorios=" + directorios + ", finTarea="
				+ finTarea + "]";
	}

}
