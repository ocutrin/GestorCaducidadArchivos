package com.caducidadarchivos.entidad;

import com.caducidadarchivos.base.EntidadJson;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Directorio extends EntidadJson {

	private static final long serialVersionUID = 8862787813278275129L;
	
	private String ruta;
	
	private int caducidad;

	@Override
	public String toString() {
		return "Directorio [ruta=" + ruta + ", caducidad=" + caducidad + "]";
	}

}
