package com.caducidadarchivos.aplicacion;

import com.caducidadarchivos.excepcion.CaducidadException;
import com.caducidadarchivos.log.Mensajes;

public enum Argumento {

	CREAR_ARCHIVO, INICIAR_TAREA, FINALIZAR_TAREA, ADD_DIRECTORIO;

	/**
	 * Es el sufijo con el que empiezan los parametros.
	 */
	public static final String SUFIJO_ARGUMENTO = "--";

	/**
	 * Es el symbolo con el que se separan las palabras de los parametro a la hora
	 * de obtener el argumento de tipo enum.
	 */
	public static final String SEPARADOR_PALABRA_ARGUMENTO = "_";

	/**
	 * Es el symbolo con el que se separan las palabras de los parametro a la hora
	 * de ejecutar el programa.
	 */
	public static final String SEPARADOR_PALABRA_ARGUMENTO_GUION = "-";

	/**
	 * Metodo que nos devuleve el Argumento desde uns String.
	 * 
	 * @param argumento
	 * @return
	 * @throws CaducidadException 
	 */
	public static Argumento getArgumento(String argumento) throws CaducidadException {
		try {
			return Argumento.valueOf(argumento.replaceFirst(SUFIJO_ARGUMENTO, "").toUpperCase()
					.replaceAll(SEPARADOR_PALABRA_ARGUMENTO_GUION, SEPARADOR_PALABRA_ARGUMENTO_GUION));
		} catch (IllegalArgumentException e) {
			throw new CaducidadException(Mensajes.ARGUMENTO_INVALIDO);
		}
	}

}
