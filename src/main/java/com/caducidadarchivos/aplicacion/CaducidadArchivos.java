package com.caducidadarchivos.aplicacion;

import java.io.File;
import java.time.LocalDate;
import java.util.LinkedHashSet;

import com.caducidadarchivos.base.Clase;
import com.caducidadarchivos.entidad.Datos;
import com.caducidadarchivos.entidad.Directorio;
import com.caducidadarchivos.excepcion.CaducidadException;
import com.caducidadarchivos.log.Log;
import com.caducidadarchivos.log.Mensajes;

public class CaducidadArchivos extends Clase {

	/**
	 * Metodo para iniciar la caducidad de archivos y actuar segun el argumento.
	 * 
	 * @param argumentos
	 */
	public void iniciar(String[] argumentos) {
		try {
			comprobarNumeroArgumentos(0, argumentos);
			comprobarFormatoArgumento(argumentos[0]);
			accionSegunTipoArgumento(Argumento.getArgumento(argumentos[0]), argumentos);
		} catch (CaducidadException e) {
			log.error(e.getMessage());
		}
	}

	private void comprobarFormatoArgumento(String argumento) throws CaducidadException {
		if (!argumento.startsWith(Argumento.SUFIJO_ARGUMENTO)) {
			throw new CaducidadException(Mensajes.ARGUMENTO_INVALIDO);
		}
	}

	private void accionSegunTipoArgumento(Argumento argumento, String[] argumentos) throws CaducidadException {
		switch (argumento) {
		case CREAR_ARCHIVO:
			comprobar(1, argumento, argumentos);
			crearArchivo(getArg(1, argumentos));
			break;
		case INICIAR_TAREA:
			if (argumentos.length > 2) {
				comprobar(2, argumento, argumentos);
				new TareaComprobacion(getArg(1, argumentos), getBooleanArgumento(2, argumentos), 10000).iniciarTarea();
			} else {
				comprobar(1, argumento, argumentos);
				new TareaComprobacion(getArg(1, argumentos)).iniciarTarea();
			}
			break;
		case FINALIZAR_TAREA:
			comprobar(1, argumento, argumentos);
			finlaizarTarea(getArg(1, argumentos));
			break;
		case ADD_DIRECTORIO:
			comprobarNumeroArgumentos(3, argumentos);
			log.info(Mensajes.ACCION, argumento.name());
			addDirectorio(getArg(1, argumentos), getArg(2, argumentos), getIntegerArgumento(3, argumentos));
			break;
		}
	}

	private void comprobar(int posicion, Enum<?> argumento, String[] argumentos) throws CaducidadException {
		comprobarNumeroArgumentos(posicion, argumentos);
		log.info(Mensajes.ACCION, argumento.name());
	}

	private boolean getBooleanArgumento(int posicion, String[] argumentos) throws CaducidadException {
		if (!"true".equals(argumentos[posicion]) && !"false".equals(argumentos[posicion])) {
			throw new CaducidadException(Mensajes.ARGUMENTO_INVALIDO);
		}
		return Boolean.parseBoolean(getArg(posicion, argumentos));
	}

	private int getIntegerArgumento(int posicion, String[] argumentos) throws CaducidadException {
		try {
			return Integer.parseInt(getArg(posicion, argumentos));
		} catch (NumberFormatException e) {
			throw new CaducidadException(Mensajes.ARGUMENTO_INVALIDO);
		}
	}

	private void comprobarNumeroArgumentos(int posicion, String[] argumentos) throws CaducidadException {
		if (argumentos.length <= posicion) {
			throw new CaducidadException(Log.formatear(Mensajes.NUMERO_ARGUMENTOS_INVALIDO, posicion + 1));
		}
	}

	private void crearArchivo(String nombreArchivoDatos) throws CaducidadException {
		CaducidadArchivosHelper.crearArchivoDatos(nombreArchivoDatos);
		CaducidadArchivosHelper.guardarDatos(
				new Datos(nombreArchivoDatos, LocalDate.now(), new LinkedHashSet<>(), false), nombreArchivoDatos);
	}

	private void finlaizarTarea(String nombreArchivoDatos) throws CaducidadException {
		Datos datos = CaducidadArchivosHelper.leerArchivoDatos(nombreArchivoDatos);
		datos.setFinTarea(true);
		CaducidadArchivosHelper.guardarDatos(datos, nombreArchivoDatos);
	}

	private void addDirectorio(String nombreArchivoDatos, String directorio, int tiempo) throws CaducidadException {
		Datos datos = CaducidadArchivosHelper.leerArchivoDatos(nombreArchivoDatos);
		if (!new File(directorio).exists()) {
			throw new CaducidadException(Log.formatear(Mensajes.EL_DIRECTORIO_NO_EXISTE));
		}
		datos.getDirectorios().add(new Directorio(directorio, tiempo));
		CaducidadArchivosHelper.guardarDatos(datos, nombreArchivoDatos);
	}

	private String getArg(int pos, String[] argumentos) {
		if (argumentos.length > pos) {
			return argumentos[pos];
		}
		return "";
	}
}
