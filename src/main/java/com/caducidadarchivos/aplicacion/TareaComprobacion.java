package com.caducidadarchivos.aplicacion;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import com.caducidadarchivos.base.Clase;
import com.caducidadarchivos.entidad.Datos;
import com.caducidadarchivos.entidad.Directorio;
import com.caducidadarchivos.excepcion.CaducidadException;
import com.caducidadarchivos.log.Mensajes;

public class TareaComprobacion extends Clase implements Runnable {

	/**
	 * Nombre del archvivo datos, para cargar y leer.
	 */
	private String nombreArchivoDatos;

	/*
	 * Flag par indicar si la tarea es a modo de servicio.
	 */
	private boolean servicio;

	/*
	 * Tiempo de delay de la tarea en milisegundos.
	 */
	private int tiempoRepeticion;

	/**
	 * Constructor
	 * 
	 * @param nombreArchivoDatos
	 */
	public TareaComprobacion(String nombreArchivoDatos) {
		this(nombreArchivoDatos, false, 0);
	}

	/**
	 * Constructor
	 * 
	 * @param nombreArchivoDatos
	 * @param servicio
	 * @param tiempoRefresco
	 */
	public TareaComprobacion(String nombreArchivoDatos, boolean servicio, int tiempoRefresco) {
		this.nombreArchivoDatos = nombreArchivoDatos;
		this.servicio = servicio;
		this.tiempoRepeticion = tiempoRefresco;
	}

	/**
	 * Metodo para iniciar la tarea de comprobacion.
	 */
	public void iniciarTarea() {
		new Thread(this).start();
	}

	@Override
	public void run() {
		boolean finTarea = false;
		if (servicio) {
			while (!finTarea) {
				try {
					Datos datos = leerDatos();
					if (isFinTareaArchivoDatos(datos)) {
						finTarea = true;
					} else {
						tarea(datos);
					}
				} catch (CaducidadException e) {
					log.error(e.getMessage());
					finTarea = true;
				}
				esperar(tiempoRepeticion);
			}
		} else {
			try {
				tarea(leerDatos());
			} catch (CaducidadException e) {
				log.error(e.getMessage());
			}
		}
	}

	private boolean isFinTareaArchivoDatos(Datos datos) {
		return datos.isFinTarea();
	}

	private void tarea(Datos datos) {
		try {
			log.info(datos);
			comprobarArchivos(datos.getDirectorios());
		} catch (CaducidadException e) {
			log.error(e.getMessage());
		}
	}

	private Datos leerDatos() throws CaducidadException {
		return CaducidadArchivosHelper.leerArchivoDatos(nombreArchivoDatos);
	}

	private void comprobarArchivos(Set<Directorio> directorios) throws CaducidadException {
		for (Directorio d : directorios) {
			recorrerDirectorio(d);
		}
	}

	private void recorrerDirectorio(Directorio directorio) throws CaducidadException {
		for (File f : new File(directorio.getRuta()).listFiles()) {
			comprobarArchivoCaducado(f, directorio.getCaducidad());
		}
	}

	private void comprobarArchivoCaducado(File archivo, int caducidad) throws CaducidadException {
		try {
			AtomicReference<Long> duracion = new AtomicReference<>(0l);
			boolean caducado = CaducidadArchivosHelper.caducado(caducidad, archivo, duracion::set);
			log.info(Mensajes.ARCHIVO_INFO_CADUCIDAD, archivo.getPath(), caducado, duracion.get());
			if (caducado) {
				Files.deleteIfExists(archivo.toPath());
				log.info(Mensajes.EL_ARCHIVO_ESTA_CADUCAD_Y_SERA_ELIMINADO, archivo.getPath());
			}
		} catch (CaducidadException e) {
			throw e;
		} catch (SecurityException e) {
			throw new CaducidadException(Mensajes.ERROR_AL_COMPROBAR_ARCHIVO_CADUCADO + ". "
					+ Mensajes.ERROR_AL_ELIMINAR_ARCHIVO + ". " + Mensajes.NO_TIENE_PERMISOS);
		} catch (IOException e) {
			throw new CaducidadException(Mensajes.ERROR_AL_COMPROBAR_ARCHIVO_CADUCADO);
		}
	}

	private void esperar(int tiempo) {
		try {
			Thread.sleep(TimeUnit.DAYS.toMillis(tiempo));
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}

}
