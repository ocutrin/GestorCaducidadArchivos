package com.caducidadarchivos.aplicacion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.function.Consumer;

import com.caducidadarchivos.entidad.Datos;
import com.caducidadarchivos.excepcion.CaducidadException;
import com.caducidadarchivos.log.Mensajes;
import com.google.gson.Gson;

public class CaducidadArchivosHelper {

	private CaducidadArchivosHelper() {
	}

	/**
	 * Metodo para crear el archivo de datos, donde se alamacenaran los directorios
	 * y tiempos para comprobar la caducidad.
	 * 
	 * @param nombre
	 *            del archivo datos a crear.
	 * @return booleano indicando si se ha creado el archivo.
	 * @throws CaducidadException
	 * @throws IOException
	 */
	public static boolean crearArchivoDatos(String nombre) throws CaducidadException {
		try {
			return new File(nombre).createNewFile();
		} catch (SecurityException e) {
			throw new CaducidadException(Mensajes.ERROR_AL_CREAR_ARCHIVO + ". " + Mensajes.NO_TIENE_PERMISOS);
		} catch (IOException e) {
			throw new CaducidadException(Mensajes.ERROR_AL_CREAR_ARCHIVO);
		}
	}

	/**
	 * Metodo para leer el archivo datos y devolver un objeto de tipo Datos.
	 * 
	 * @param nombre
	 * @return
	 * @throws CaducidadException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static Datos leerArchivoDatos(String nombre) throws CaducidadException {
		try (FileReader fileReader = new FileReader(new File(nombre));
				BufferedReader bufferedReader = new BufferedReader(fileReader)) {
			StringBuilder stringBuilder = new StringBuilder();
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				stringBuilder.append(line);
			}
			return getDatos(stringBuilder);
		} catch (FileNotFoundException e) {
			throw new CaducidadException(Mensajes.ERROR_AL_LEER_ARCHIVO + ". " + Mensajes.EL_ARCHIVO_NO_EXISTE);
		} catch (IOException e) {
			throw new CaducidadException(Mensajes.ERROR_AL_LEER_ARCHIVO);
		}
	}

	private static Datos getDatos(StringBuilder stringBuilder) {
		return new Gson().fromJson(stringBuilder.toString(), Datos.class);
	}

	/**
	 * Metodo para guardar en un archivo datos un objeto de tipo datos.
	 * 
	 * @param datos
	 * @param nombre
	 * @throws CaducidadException
	 * @throws FileNotFoundException
	 */
	public static void guardarDatos(Datos datos, String nombre) throws CaducidadException {
		try (PrintWriter printWriter = new PrintWriter(new File(nombre))) {
			printWriter.print(datos.toJsonIndentado());
		} catch (FileNotFoundException e) {
			throw new CaducidadException(Mensajes.ERROR_AL_GUARDAR_ARCHIVO + ". " + Mensajes.EL_ARCHIVO_NO_EXISTE);
		}
	}

	/**
	 * Metodo que devuelve si un archivo esta caducado respecto a la fecha de hoy,
	 * segun el tiempo pasado.
	 * 
	 * @param tiempo
	 * @param archivo
	 * @return
	 * @throws CaducidadException
	 * @throws IOException
	 */
	public static boolean caducado(int tiempo, File archivo, Consumer<Long> duracion) throws CaducidadException {
		long d = Duration.between(getInstanteCreacion(archivo), getInstanteAhora()).toDays();
		duracion.accept(d);
		return d > tiempo;
	}

	/**
	 * Metodo que devuelve el instante ahora.
	 * 
	 * @return
	 */
	public static Instant getInstanteAhora() {
		return Clock.systemUTC().instant();
	}

	/**
	 * Metodo que devuelve el instante de creacion del archivo.
	 * 
	 * @return
	 * @throws CaducidadException
	 */
	public static Instant getInstanteCreacion(File archivo) throws CaducidadException {
		return getArchivoAtributos(archivo).lastModifiedTime().toInstant();
	}

	private static BasicFileAttributes getArchivoAtributos(File archivo) throws CaducidadException {
		try {
			return Files.readAttributes(archivo.toPath(), BasicFileAttributes.class);
		} catch (SecurityException e) {
			throw new CaducidadException(Mensajes.ERROR_AL_LEER_ATRIBUTOS_ARCHIVO + ". " + Mensajes.NO_TIENE_PERMISOS);
		} catch (IOException e) {
			throw new CaducidadException(Mensajes.ERROR_AL_LEER_ATRIBUTOS_ARCHIVO);
		}
	}

}
