package com.caducidadarchivos.excepcion;

public class CaducidadError extends Error {
	
	private static final long serialVersionUID = 7851303876733158989L;

	public CaducidadError() {
		super();
	}

	public CaducidadError(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public CaducidadError(String message, Throwable cause) {
		super(message, cause);
	}

	public CaducidadError(String message) {
		super(message);
	}

	public CaducidadError(Throwable cause) {
		super(cause);
	}

}
