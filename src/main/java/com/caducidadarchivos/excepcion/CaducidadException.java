package com.caducidadarchivos.excepcion;

public class CaducidadException extends Exception {

	private static final long serialVersionUID = -1626407487127834775L;

	public CaducidadException() {
		super();
	}

	public CaducidadException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public CaducidadException(String message, Throwable cause) {
		super(message, cause);
	}

	public CaducidadException(String message) {
		super(message);
	}

	public CaducidadException(Throwable cause) {
		super(cause);
	}

}
