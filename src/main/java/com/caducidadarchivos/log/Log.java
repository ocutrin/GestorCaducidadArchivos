package com.caducidadarchivos.log;

import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Log {

	private final Logger logger;

	private Log(Class<?> clazz) {
		logger = Logger.getLogger(clazz.getName());
	}

	public static Log getLog(Class<?> clazz) {
		return new Log(clazz);
	}

	public void info(Object mensaje, Object... parametros) {
		log(Level.INFO, formatear(mensaje, parametros));
	}

	public void error(Object mensaje, Object... parametros) {
		log(Level.SEVERE, formatear(mensaje, parametros));
	}

	private void log(Level level, Object objeto) {
		if (logger.isLoggable(level)) {
			logger.log(level, objeto.toString());
		}
	}

	public static final String formatear(Object mensaje, Object... parametros) {
		return MessageFormat.format(mensaje.toString(), parametros);
	}

}
