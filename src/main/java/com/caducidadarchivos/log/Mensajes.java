package com.caducidadarchivos.log;

public class Mensajes {

	private Mensajes() {
	}

	public static final String ACCION = "Accion: {0}";

	public static final String ARGUMENTO_INVALIDO = "Argumento/s invalido/s";

	public static final String NUMERO_ARGUMENTOS_INVALIDO = "Numero de argumentos invalido. Se esperaban {0} argumentos";

	public static final String ERROR_AL_CREAR_ARCHIVO = "Error al crear el archivo";

	public static final String ERROR_AL_GUARDAR_ARCHIVO = "Error al guardar datos en el archivo";
	
	public static final String ERROR_AL_ELIMINAR_ARCHIVO = "Error al eliminar archivo";

	public static final String ERROR_AL_LEER_ARCHIVO = "Error al leer datos del archivo";

	public static final String ERROR_AL_LEER_ATRIBUTOS_ARCHIVO = "Error al leer atributos del archivo";
	
	public static final String ERROR_AL_COMPROBAR_ARCHIVO_CADUCADO = "Error al comprobar el archivo caducado";

	public static final String NO_TIENE_PERMISOS = "No tiene permisos";

	public static final String EL_DIRECTORIO_NO_EXISTE = "El directorio no existe";

	public static final String EL_ARCHIVO_NO_EXISTE = "El archivo no existe";
	
	public static final String EL_ARCHIVO_ESTA_CADUCAD_Y_SERA_ELIMINADO = "El archivo {0} esta caducado y se ha eliminado";
	
	public static final String ARCHIVO_INFO_CADUCIDAD = "Archivo [nombre={0}, caducado={1}, diferencia={2}]";

}
