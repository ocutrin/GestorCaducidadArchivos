package com.caducidadarchivos.ejecutable;

import com.caducidadarchivos.aplicacion.CaducidadArchivos;

public class Main {
	public static void main(String[] args) {
		new CaducidadArchivos().iniciar(args);
	}
}
